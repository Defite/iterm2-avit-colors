# Hey!

This repository contains color presets for 'oh my zsh' Avit theme for Iterm2. 

![Iterm2 Avit colors](https://user-images.githubusercontent.com/299118/35504338-dfd81730-04f3-11e8-8624-c6b8e3a8ccda.png)

## Install

Go to Iterm2 -> Preferences -> Profile -> Colors -> Color Presets and Import `iterm2-zsh-avit-theme-colors`. After that go to Preferences -> Appearence and set things like on screenshot:

![Iterm2 Settings](https://user-images.githubusercontent.com/299118/35504544-649fd91c-04f4-11e8-95aa-d5a78519afe5.png)


